# Autoware Open AD Kit Blueprint v1.1
## Overview

Since ADLINK's [AVA Developer Platform](https://www.ipi.wiki/pages/com-hpc-altra)
is powerful with 32 Neoverse N1 cores, it's feasible to run all autoware
components (includes Open AD Kit v1.0, RViz and Scenario Simulator) on
the AVA Developer Platform with EWAOL.

In this guide a user will setup self-contained envoironment by running
all components on an AVA Developer Platform, which can reduce the
deployment complexity and dependencies for CI/CD.

<img src="./images/oak_bp_v1_1.jpg" alt="oak_bp_v1_1" width="600"/>

### Prerequisites

The AVA Developer Platform is the only required machine, the steps in
this guide assumes the following:

* two NVMe M.2 SSDs are installed, one SSD is for pre-installed Ubuntu,
  another SSD is for installation EWAOL system on the fly.
* running Ubuntu 22.04 LTS
  * [ADLINK's Ubuntu installation](https://www.ipi.wiki/pages/comhpc-docs?page=UbuntuInstallation.html)
  * [Ubuntu download for Arm](https://ubuntu.com/download/server/arm)
  * [Ubuntu installation](https://ubuntu.com/tutorials/create-a-usb-stick-on-ubuntu#1-overview)
* internet connection
* a serial connection
  * [Serial console](https://www.ipi.wiki/pages/comhpc-docs?page=HowToUseSerialConsole.html)
* a `ssh` connection
* a display connection with VGA port

Please ensure that the prerequisites are met prior to continuing to
ensure a smooth bring-up.

## EWAOL

The Arm build machine used in this guide is the AVA Developer Platform
and is running Ubuntu 22.04 LTS. Start by installing software
dependecies needed for building and flashing the EWAOL image.

``` On AVA Developer Platform Ubuntu
sudo apt-get update
sudo apt-get install -y python3-pip bmaptools chrpath diffstat liblz4-tool
```

`kas` is used to build the EWAOL image for the AVA Developer Platform.
Comparing to Autoware Open AD Kit Blueprint v1.0, this version (v1.1)
builds Xorg and xfce for graphics system so that RViz GUI application
can run on the EWAOL system.

``` On AVA Developer Platform Ubuntu
git clone -b kirkstone-dev https://gitlab.com/Linaro/ewaol/meta-ewaol-machine
cd meta-ewaol-machine
kas build kas/ewaol/baremetal.yml:kas/machine/avadp-xfce.yml
```

Once the build is complete, `ewaol-baremetal-image-ava.wic.gz` and
`ewaol-baremetal-image-ava.wic.bmap` files can be found in the
`build/tmp_baremetal/deploy/images/ava/` directory. These are the files
which will be used to flash EWAOL on the AVA Developer Platform.

## AVA Developer Platform Bring-Up

The AVA Developer Platform is configured with two M.2 NVMes SSDs, which
allows us to run two different OSes.

* Ubuntu 22.04 LTS
  * Used to build the EWAOL image (previous section) and flash the other SSD with the image
* EWAOL
  * Used to run Open AD Kit v1.0

### Flash the EWAOL image

Start by identifying the SSD to be flashed.

``` On AVA Developer Platform Ubuntu
lsblk
```

Then use `bmap-tools` to flash the SSD, don't forget to replace
`<SSD_NAME>` below.

``` On AVA Developer Platform Ubuntu
$ sudo bmaptool copy --bmap build/tmp_baremetal/deploy/images/ava/ewaol-baremetal-image-ava.wic.bmap build/tmp_baremetal/deploy/images/ava/ewaol-baremetal-image-ava.wic.gz /dev/<SSD_NAME>
```

An example of a succesfull flash on a 128GB SSD is shown below.

``` On AVA Developer Platform Ubuntu
$ lsblk /dev/nvme0n1
NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
nvme0n1     259:0    0 119.2G  0 disk 
├─nvme0n1p1 259:5    0  40.1M  0 part 
└─nvme0n1p2 259:6    0   3.4G  0 part 
```

Next we wish to grow the filesystem partition to be able to make use of
the unallocated space, don't forget to replace `<SSD_NAME>`,
`<MOUNT_POINT>` and `<SSD_NAME_P2>` below.

```
sudo mount /dev/<SSD_NAME_P2> <MOUNT_POINT>
sudo growpart /dev/<SSD_NAME> 2
sudo resize2fs /dev/<SSD_NAME_P2>
```

Below is an example of a successful resize of the filesystem.

```
$ lsblk /dev/nvme0n1
NAME                      MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
nvme0n1                   259:0    0 119.2G  0 disk 
├─nvme0n1p1               259:5    0  40.1M  0 part 
└─nvme0n1p2               259:6    0 119.2G  0 part /media
```

Now reboot and in the next section, we'll use `Boot Manager` to boot
EWAOL.

```
sudo reboot
```

### Boot EWAOL

Using the serial console, when the following text in the boot log

```
Tianocore/EDK2 firmware version 2.04.100.07 Build 20220908 ATF 2.06
Press ESCAPE for boot options PROGRESS CODE: V02020000 I0
```

is visible, hit `Esc`. Then select `Boot Manager`.

```
 Ampere(R) Altra(R) Processor                        1.50 GHz                  
 2.04.100.07.A1 Build 20220908 ATF 2.06              32513 MB RAM              
                                                                               
                                                                               
                                                                               
   Select Language            <Standard English>         This selection will    
                                                         take you to the Boot  
 > Device Manager                                        Manager                
 > Boot Manager                                                                
 > Boot Maintenance Manager                                                    
                                                                               
   Continue                                                                    
   Reset                                                                        
```

Next select the EWAOL boot option, in the example below, that's
`UEFI OS (ADATA_IM2P33F8-128GCTB4)`.

```
/------------------------------------------------------------------------------\
|                                Boot Manager                                  |
\------------------------------------------------------------------------------/
                                                                                
                                                         Device Path :          
   Boot Manager Menu                                     PcieRoot(0x7)/Pci(0x5, 
                                                         0x0)/Pci(0x0,0x0)/NVMe 
   UEFI OS (ADATA_IM2P33F8-128GCTB4)                     (0x1,01-00-00-00-00-00 
   ubuntu                                                -00-00)/CDROM(0x0,0x2A 
   UEFI PXEv4 (MAC:0030643B505C)                         9F40,0xAC00)/\EFI\BOOT 
   UEFI PXEv6 (MAC:0030643B505C)                         \bootaa64.efi          
   UEFI HTTPv4 (MAC:0030643B505C)                                               
   UEFI HTTPv6 (MAC:0030643B505C)                                               
   UEFI Shell                                                                   
   Ubuntu (TS256GMTE110S)                                                       
                                                                                
   Use the <^> and <v> keys to choose a boot option,                            
   the <Enter> key to select a boot option, and the                             
   <Esc> key to exit the Boot Manager Menu.                                     
```

Then either just wait or hit `Enter` once in the `GNU GRUB` menu.

```
                             GNU GRUB  version 2.06

 /----------------------------------------------------------------------------\
 |*PARTUUID Boot: COM-HPC AVA Yocto Image                                     |
 | NVMe M.2  SSD Boot: COM-HPC AVA Yocto Image                                |
 | USB Boot (If Drive is present): COM-HPC AVA Yocto Image                    |
 |                                                                            |
 \----------------------------------------------------------------------------/

      Use the ^ and v keys to select which entry is highlighted.          
      Press enter to boot the selected OS, `e' to edit the commands      
      before booting or `c' for a command-line.    
```

Log in with user `ewaol` (no password) and find its IP address

```
ifconfig
```

and make a note of it.

### Display Setup

After EWAOL system booting up, we need to launch xfce4 windows manager
with below command in the console, then the graphics system is displayed
via VGA port and we see it from the connected monitor.

```
# startxfce4 &
```

### Autoware Setup

Start connecting to the AVA using `ssh`, don't forget to replace
`<IP_ADDRESS>`.

```
ssh ewaol@<IP_ADDRESS>
```

Add user `ewaol` into the docker group.

```
sudo usermod -aG docker ${USER}
```

You would need to log out and log in again so the updated group
membership is re-evaluated:

```
exit

ssh ewaol@<IP_ADDRESS>
```

Then download the convenience script `bp_oak_v1_0_ava_install_pkgs.sh`

```
wget wget https://gitlab.com/soafee/blueprints/-/raw/bp-openadkit-v1_0/autoware_open_ad_kit_blueprint/r1/oak_bp_v1_0_ava_install_pkgs.sh
```

and run it.

```
chmod +x bp_oak_v1_0_ava_install_pkgs.sh
./bp_oak_v1_0_ava_install_pkgs.sh
```

The convenience script downloads necessary files, configures CycloneDDS
and pulls the Open AD Kit v1.0 container.

```
$ docker images
REPOSITORY                                                                               TAG       IMAGE ID       CREATED         SIZE
registry.gitlab.com/autowarefoundation/autoware.auto/autowareauto/arm64/openadkit-foxy   latest    48a4503b4fe4   11 months ago   6.65GB
```

The autoware image is now configured correctly!

## RViz and Scenario Simulator Setup

It's now time to install RViz and Scenario Simulator on the AVA
platform. Start by pulling the Docker image for Arm64 version.

```
$ docker pull tier4/scenario_simulator_v2:open_ad_kit-arm64-foxy
```

After finished downloading, below command can be used to check the
installed docker image:

```
$ docker image ls
REPOSITORY                      TAG                      IMAGE ID       CREATED         SIZE
tier4/scenario_simulator_v2     open_ad_kit-arm64-foxy   530eba917852   2 months ago    4.34GB
```

Create a `scenario` folder in your home directory and download the
scenario files.

```
mkdir -p ~/scenario
pushd ~/scenario
wget https://gitlab.com/autowarefoundation/autoware_reference_design/-/blob/main/docs/Appendix/Open-AD-Kit-Start-Guide/scenario/scenario_e3b743e7-110c-4db6-b136-e5ffd5538315_2.yml
wget https://gitlab.com/autowarefoundation/autoware_reference_design/-/blob/main/docs/Appendix/Open-AD-Kit-Start-Guide/scenario/scenario_a7effa60-c07d-4df4-b082-bc0d6cbae825_1.yml
popd
```

The [Agile Development Environment (ADE)]() is required to deploy RViz
GUI application, and the Scenario Simulator depends on it for showing
vehicle movement.  Install it using the instructions below.

```
$ wget https://gitlab.com/ApexAI/ade-cli/-/jobs/1859684349/artifacts/raw/dist/ade+aarch64 -O ade
$ chmod +x ade
```

Update ADE to the latest version
```
$ ./ade update-cli
```

and move it to `usr/bin`.
```
$ sudo mv ade /usr/bin/
```

Create `adehome` and clone the Autoware.Auto repository.

```
$ mkdir -p ~/adehome
$ pushd ~/adehome
$ touch .adehome
$ git clone https://gitlab.com/autowarefoundation/autoware.auto/AutowareAuto.git
$ popd
```

All preparations are now complete, well done!

By now, you have set up all dependencies on the AVA Developer Platform
and it's time to run Open AD Kit v1.0, RViz and the Scenario Simulator
on it!

### Run OpenAD kit demonstration On AVA Developer Platform

Launch Autoware modules.

```
$ docker run --rm -it --net host -v ~/map:/map -v ~/cyclonedds:/etc/cyclonedds 48a4503b4fe4 /bin/bash -c "export CYCLONEDDS_URI=file:///etc/cyclonedds/cyclonedds.xml; export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp; source install/setup.bash; ros2 launch scenario_simulator_launch autoware_auto_mapping.launch.py map_path:=/map/kashiwanoha" &
$ sleep 5 
$ docker run --rm -it --net host -v ~/cyclonedds:/etc/cyclonedds 48a4503b4fe4 /bin/bash -c "export CYCLONEDDS_URI=file:///etc/cyclonedds/cyclonedds.xml; export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp; source install/setup.bash; ros2 launch scenario_simulator_launch autoware_auto_perception.launch.py" &
$ sleep 5 
$ docker run --rm -it --net host -v ~/cyclonedds:/etc/cyclonedds 48a4503b4fe4 /bin/bash -c "export CYCLONEDDS_URI=file:///etc/cyclonedds/cyclonedds.xml; export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp; source install/setup.bash; ros2 launch scenario_simulator_launch autoware_auto_planning.launch.py" &
$ sleep 5 
$ docker run --rm -it --net host -v ~/cyclonedds:/etc/cyclonedds 48a4503b4fe4 /bin/bash -c "export CYCLONEDDS_URI=file:///etc/cyclonedds/cyclonedds.xml; export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp; source install/setup.bash; ros2 launch autoware_auto_launch autoware_auto_vehicle.launch.py" &
```

Launch the Docker container using ADE.

```
$ cd ~/adehome/AutowareAuto
$ ade --rc .aderc-amd64-foxy-lgsvl start --update --enter
```

Then run the visialization once inside the Docker container.

```
$ source /opt/AutowareAuto/setup.bash
$ ros2 launch autoware_auto_launch autoware_auto_visualization.launch.py
```

Finally, launch the Scenario Simulator container.

```
$ docker run --rm -it --net host -v /home/${USER}/scenario:/scenario -v /home/${USER}/cyclonedds:/etc/cyclonedds 530eba917852 /bin/bash -c "export CYCLONEDDS_URI=file:///etc/cyclonedds/cyclonedds.xml; export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp; apt-get update; apt-get install -y ros-foxy-rmw-cyclonedds-cpp; source install/setup.bash; ros2 launch scenario_test_runner scenario_test_runner.launch.py sensor_model:=aip_xx1 vehicle_model:=lexus launch_autoware:=false architecture_type:=awf/auto scenario:=/scenario/scenario_e3b743e7-110c-4db6-b136-e5ffd5538315_2.yml"
```
